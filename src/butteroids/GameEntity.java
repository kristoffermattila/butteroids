package butteroids;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.UnsupportedAudioFileException;

public abstract class GameEntity {

	protected final GameModel gameModel;
    protected double xVelocity;
    protected double yVelocity;
    private Point2D position;
    protected boolean active;
    protected Path2D baseShape;
    protected Color color;
	protected boolean solid;
    protected SoundPlayer soundPlayer;
	protected double rotation;
	protected double rotationSpeed;

    public GameEntity(double x, double y, SoundPlayer sp, GameModel gm) {
		gameModel = gm;
        rotation = 0;
		xVelocity = 0;
		yVelocity = 0;
        position = new Point2D.Double(x, y);
        active = true;
        color = Color.WHITE;
		solid = false;
		soundPlayer = sp;
    }
	
	public double getX() {
		return position.getX();
	}

	public double getY() {
		return position.getY();
	}
	
	
	
    public void move(double x, double y) {
        position.setLocation(getX()+x, getY()+y);
    }

    public void render(Graphics2D g) {
		AffineTransform at = AffineTransform.getRotateInstance(Math.toRadians(rotation), getX(), getY());
		at.concatenate(AffineTransform.getTranslateInstance(getX(), getY()));
		Path2D transformedShape = (Path2D) baseShape.createTransformedShape(at);
        g.setColor(color);
		if(solid) {
			g.fill(transformedShape);
		} else {
			g.draw(transformedShape);
		}
    }

	public void setPosition(Point2D p) {
		position.setLocation(p);
	}
	
	/**
	 * It just moves and rotates according to velocity and rotationSpeed
	 * @param delta 
	 */
    public void tick(double delta) {
		move(xVelocity*delta, yVelocity*delta);
        rotate(rotationSpeed*delta);
    }

	public void rotate(double degrees) {
		rotation = (rotation + degrees);
		while (rotation < 0.0) {
			rotation += 360;
		}
		while (rotation >= 360.0) {
			rotation -= 360;
		}
	}

	void loopInWorld(int xBoundary, int yBoundary) {
		double x=getX(), y=getY();
		while(x < 0) {
			x += xBoundary;
		}
		while(x > xBoundary) {
			x -= xBoundary;
		}
		while(y < 0) {
			y += yBoundary;
		}
		while(y > yBoundary) {
			y -= yBoundary;
		}
		position.setLocation(x, y);
	}
	
	public abstract void die();
}
