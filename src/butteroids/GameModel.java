package butteroids;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.stream.Stream;

public class GameModel {
	static private int xBoundary = 640;
	static private int yBoundary = 480;
    boolean running;
	private SoundPlayer soundPlayer;
    final PlayerShip playerShip1;
	private ArrayList<Butteroid> butts;
	private ArrayList<Bullet> bullets;

    public GameModel(SoundPlayer sp) {
		soundPlayer = sp;
        playerShip1 = new PlayerShip(xBoundary/2, yBoundary/2, soundPlayer, this);
		butts = new ArrayList<>();
		butts.add(new Butteroid(xBoundary/2, yBoundary/5, Math.random()%50, Math.random()*5, Math.random()*5, soundPlayer, this));
		bullets = new ArrayList<>();
    }
	
	/**
	 * Is this javadoc?
	 * @param ge 
	 */
	public void loopEntity(GameEntity ge) {
		ge.loopInWorld(xBoundary, yBoundary);
	}

    public void render(Graphics2D g) {
		getActiveEntitiesStream().forEach(ge -> ge.render(g));
    }

    public void tick(double delta) {
		getActiveEntitiesStream().forEach(ge -> ge.tick(delta));
		getActiveEntitiesStream().forEach(ge -> loopEntity(ge));
		
		// Remove bullets whose lifetime is up
		for(Iterator<Bullet> bi = bullets.iterator(); bi.hasNext();) {
			if(bi.next().isTimeUp()) {
				bi.remove();
			}
		}
		
    }
	
	private Stream<GameEntity> getActiveEntitiesStream() {
		Stream<GameEntity> s = Stream.concat(Stream.concat(Stream.of(playerShip1), butts.stream()), bullets.stream());
		return s;
	}
	
	// Just for debug
	public void killButt() {
		butts.get(butts.size()-1).die();
	}

	void thisWasShot(Bullet b) {
		bullets.add(b);
	}
	
}
