/*
 * Copyright © 2018 Kristoffer Mattila <kristoffer.mattila@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package butteroids;

/**
 *
 * @author Kristoffer Mattila
 */
public class GameModelMessage {
	public enum Type {
		SHOOT
	}
	
	public class ShootEvent {
		
	}
	
	public Type type;
	
}
