/*
 * Copyright © 2018 Kristoffer Mattila <kristoffer.mattila@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package butteroids;

import java.awt.geom.Path2D;

/**
 *
 * @author Kristoffer Mattila
 */
public class Bullet extends GameEntity {
	static private double SPEED=13;
	private double lifetime;
	private final PlayerShip owner;

	public Bullet(double x, double y, double direction, SoundPlayer sp, GameModel gm, PlayerShip owner) {
		super(x, y, sp, gm);
		this.owner = owner;
		lifetime = 60;
		xVelocity = Math.sin(Math.toRadians(direction)) * SPEED;
		yVelocity = (-Math.cos(Math.toRadians(direction))) * SPEED;
		solid = true;
		baseShape = createShape();
	}
	
	public boolean isTimeUp() {
		return lifetime <= 0.0;
	}
	
	@Override
	public void tick(double delta) {
		super.tick(delta);
		lifetime -= delta;
	}

	@Override
	public void die() {
	}

	private Path2D createShape() {
		Path2D path = new Path2D.Double(Path2D.WIND_EVEN_ODD, 3);
		path.moveTo(-1, 0);
		path.quadTo(0, -1, 1, 0);
		path.quadTo(0, 1, -1, 0);
		return path;
	}
	
}
