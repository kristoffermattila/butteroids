package butteroids;

import butteroids.PlayerShip;

@FunctionalInterface
public interface ShipCommand {

    public abstract void execute(PlayerShip p);
}
