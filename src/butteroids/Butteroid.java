/*
 * Copyright © 2018 Kristoffer Mattila <kristoffer.mattila@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package butteroids;

import butteroids.SoundPlayer.SoundTag;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import static java.awt.geom.Path2D.WIND_EVEN_ODD;

/**
 *
 * @author Kristoffer Mattila
 */
public class Butteroid extends GameEntity{

	public Butteroid(double x, double y, double rotationSpeed, double xVelocity, double yVelocity, SoundPlayer sp, GameModel gm) {
		super(x, y, sp, gm);
		soundPlayer.loadSound(SoundTag.BUTT_DESTROY);
		baseShape = createShape();
		this.rotationSpeed = rotationSpeed;
		this.xVelocity = xVelocity;
		this.yVelocity = yVelocity;
	}
	
	static public Path2D createShape() {
		Path2D path = new Path2D.Double(WIND_EVEN_ODD, 5);
		path.moveTo(10, -5);
		path.curveTo(20, 10, 40, 0, 40, -15);
		path.curveTo(40, -50, -40, -50, -40, 0);
		path.curveTo(-40, 50, 40, 50, 40, 20);
		path.curveTo(40, 0, 25, 0, 20, 0);
		return path;
	}

	@Override
	public void die() {
		System.out.println("Butt died");
		soundPlayer.play(SoundTag.BUTT_DESTROY, false);
		active = false;
	}

}
