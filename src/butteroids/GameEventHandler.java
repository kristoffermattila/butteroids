package butteroids;

import java.awt.Graphics2D;

public class GameEventHandler {

    GameModel gameModel;
	SoundPlayer soundplayer;
    ShipController shipController1;

    public GameEventHandler(Butteroids b) {
		soundplayer = new SoundPlayer();
        gameModel = new GameModel(soundplayer);
        shipController1 = new ShipController(gameModel, b);
    }

    public void render(Graphics2D g) {
        gameModel.render(g);
    }

    public void tick(double delta) {
        gameModel.tick(delta);
    }
}
