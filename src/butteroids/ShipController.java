package butteroids;

import com.sun.org.glassfish.gmbal.GmbalMBean;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Array;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

public final class ShipController implements KeyListener {
	
    private class Action {
        private boolean isPressed = false;
        private int keyCode;
        private ShipCommand pressCommand;
        private ShipCommand releaseCommand;

        public Action(ShipCommand press, ShipCommand release) {
            pressCommand = press;
            releaseCommand = release;
        }

        public void setPressed(boolean b) {
            isPressed = b;
        }
    }
	
    private final GameModel gameModel;
	private final Butteroids app;
    private final PlayerShip ship;
    private Map<Integer, Action> vkeys;

//    private Action right;

    public ShipController(GameModel g, Butteroids b) {
		vkeys = new HashMap<>(4);
        gameModel = g;
        ship = g.playerShip1;
        setP1Default();
		app = b;
        b.addKeyListener(this);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        final int key = e.getExtendedKeyCode();
        Action action = vkeys.get(key);
		if(action != null && !action.isPressed) {
			action.pressCommand.execute(ship);
			action.setPressed(true);
		}
    }
	// Shit implementations
    @Override
    public void keyReleased(KeyEvent e) {
        final int key = e.getExtendedKeyCode();
        Action action = vkeys.get(key);
		if(action != null) {
			action.releaseCommand.execute(ship);
			action.setPressed(false);
		}
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    public void setP1Default() {
		vkeys.put(KeyEvent.VK_D, new Action(PlayerShip::startTurningRight, PlayerShip::stopTurningRight));
		vkeys.put(KeyEvent.VK_A, new Action(PlayerShip::startTurningLeft, PlayerShip::stopTurningLeft));
		vkeys.put(KeyEvent.VK_J, new Action(PlayerShip::shoot, ship -> {} ));
		vkeys.put(KeyEvent.VK_K, new Action(PlayerShip::startAccelerating, PlayerShip::stopAccelerating));
		
		//Shitty debug thing
		vkeys.put(KeyEvent.VK_ESCAPE, new Action(ship -> app.stop(), ship -> {}));
		vkeys.put(KeyEvent.VK_SPACE, new Action(ship -> gameModel.killButt(), ship -> {}));
    }
}
