package butteroids;

import butteroids.SoundPlayer.SoundTag;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import static java.awt.geom.Path2D.WIND_EVEN_ODD;
import java.awt.geom.Rectangle2D;
import javax.sound.sampled.Clip;

public class PlayerShip extends GameEntity {
	static private final double TURNINGSPEED = 6.3;
	static private final double MAXSPEED = 10;
	static private final double BRAKE_COEFFICIENT = 0.01;
	static private final double ACCEL_FORCE = 0.12;
	private double accel;
	private Clip thrustClip;

    public PlayerShip(double x, double y, SoundPlayer sp, GameModel gm) {
		super(x,y, sp, gm);
        rotationSpeed = 0;
		accel = 0;
		solid = true;
        baseShape = new Path2D.Double(WIND_EVEN_ODD, 3);
        baseShape.moveTo(0, -15);
        baseShape.lineTo(10, 15);
        baseShape.lineTo(-10, 15);
        baseShape.closePath();
		soundPlayer.loadSound(SoundTag.SHOT);
		soundPlayer.loadSound(SoundTag.THRUST);
    }
	
	public void accelerate(double amount, double delta) {
		if(amount != 0) { // Accelerating
			xVelocity += Math.sin(Math.toRadians(rotation)) * amount * delta;
			yVelocity += (-Math.cos(Math.toRadians(rotation))) * amount * delta;
		} else { //Not accelerating (Brake)
			xVelocity -= xVelocity * BRAKE_COEFFICIENT * delta;
			yVelocity -= yVelocity * BRAKE_COEFFICIENT * delta;
		}
		
		double speed = Math.hypot(xVelocity, yVelocity);
		if(speed > MAXSPEED) { // Limit speed
			xVelocity = xVelocity * (MAXSPEED / speed);
			yVelocity = yVelocity * (MAXSPEED / speed);
		} else if (speed < (ACCEL_FORCE/2)*delta) { // Stop jiggling at low speed
			xVelocity = 0;
			yVelocity = 0;
		}
	}


    public void shoot() {
	    soundPlayer.play(SoundTag.SHOT, false);
		gameModel.thisWasShot(new Bullet(getX(), getY(), rotation, soundPlayer, gameModel, this));
    }
	
	public void startAccelerating() {
		accel = ACCEL_FORCE;
		thrustClip = soundPlayer.play(SoundTag.THRUST, true);
	}
	
	public void stopAccelerating() {
		accel = 0;
		if(thrustClip != null) {
			soundPlayer.stop(thrustClip);
		}
	}

    public void startTurningLeft() {
        rotationSpeed -= TURNINGSPEED;
    }

    public void stopTurningLeft() {
        rotationSpeed += TURNINGSPEED;
    }
	
    public void startTurningRight() {
        rotationSpeed += TURNINGSPEED;
    }

    public void stopTurningRight() {
        rotationSpeed -= TURNINGSPEED;
    }

    @Override
    public void tick(double delta) {
		accelerate(accel, delta);
		
        super.tick(delta);
    }

	@Override
	public void die() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
}
