/*
 * Copyright © 2018 Kristoffer Mattila <kristoffer.mattila@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package butteroids;

import java.io.IOException;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Kristoffer Mattila
 */
public class SoundPlayer implements LineListener {
	private EnumMap sounds = new EnumMap(SoundTag.class);
	static final String soundPath[] = new String[] {"resources/Shot.wav", "resources/ButtDestroy.wav", "resources/Thrust.wav"};

	public Clip play(SoundTag tag, boolean looping) {
		Sound sound = (Sound) sounds.get(tag);
		Clip clip;
		if(sound == null) {
			return null;
		}
		try {
			clip = (Clip) AudioSystem.getLine(sound.info);
			clip.open(sound.format, sound.data, 0, sound.size);
			clip.addLineListener(this);
			if(looping) {
				clip.loop(Clip.LOOP_CONTINUOUSLY);
			}
			clip.start();
		} catch (LineUnavailableException ex) {
			Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
			return null;
		} 
		return clip;
	}
	
	public void stop(Clip clip) {
		clip.stop();
	}

	public boolean loadSound(SoundTag tag) {
		if(sounds.containsKey(tag)) {
			return true;
		}
		Sound sound;
		try {
			sound = new Sound(soundPath[tag.ordinal()]);
		} catch (UnsupportedAudioFileException | IOException ex) {
			Logger.getLogger(SoundPlayer.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
		sounds.put(tag, sound);
		return true;
	}

	@Override
	public void update(LineEvent le) {
		if(le.getType() == LineEvent.Type.STOP) {
			le.getLine().close();
		}
	}

	public enum SoundTag {
		SHOT, BUTT_DESTROY, THRUST
	}
	
		
}
