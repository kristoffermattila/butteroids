/*
 * Copyright © 2018 Kristoffer Mattila <kristoffer.mattila@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */
package butteroids;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 *
 * @author Kricke
 */
public class Sound {
	byte[] data;
	int size;
	AudioFormat format;
	DataLine.Info info;
	
	public Sound(String path) throws UnsupportedAudioFileException, IOException {
			AudioInputStream in = AudioSystem.getAudioInputStream(getClass().getResourceAsStream(path));
			format = in.getFormat();
			size = (int) (format.getFrameSize() * in.getFrameLength());
			data = new byte[size];
			info = new DataLine.Info(Clip.class, format, size);
			in.read(data, 0, size);
		
	}
	
}
